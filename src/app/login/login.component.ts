import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  constructor() { }

  shiftLeft:Boolean=false;
  shiftRight:Boolean=false;

  showLoginBox:Boolean=true;
  showSignUpBox:Boolean=false;


  ngOnInit() {
  }

  showLogin(){
    console.log("ShowLogin");
    this.shiftRight=true;
    this.shiftLeft=false;
    this.showLoginBox = true;
    this.showSignUpBox = false;
    console.log("shiftRight "+this.shiftRight);
    console.log("shiftLeft "+this.shiftLeft);
  }

  showSignUp(){
    console.log("showSignUp");
    this.shiftRight=false;
    this.shiftLeft=true;
    this.showLoginBox = false;
    this.showSignUpBox = true;
    console.log("shiftRight "+this.shiftRight);
    console.log("shiftLeft "+this.shiftLeft);

  }

}
